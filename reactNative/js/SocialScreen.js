import React from 'react';
import { StyleSheet, Button, Text, TextInput, View, Picker } from 'react-native';
import { StackNavigator } from 'react-navigation';
import VerificationScreen from './VerificationScreen';
import SuccessfulScreen from './SuccessfulScreen';

export default class SocialScreen extends React.Component {
  static navigationOptions = { title: 'Social Network Account', header: null };
  state={
    selected: 'Google',
    email: '',
    password: '',
  };
  
  render() {
    const { navigate } = this.props.navigation;
    return (
    <View style={{flex:1, padding: 30, paddingTop:60, backgroundColor: "white"}}>
      <Text>E-mail</Text>
      <TextInput onChangeText={(email) => this.setState({email})}/>
      <Text>Password</Text>
      <TextInput onChangeText={(password) => this.setState({password})}/>
      <Picker
        selectedValue={this.state.selected}
        onValueChange={(itemValue, itemIndex) => this.setState({selected: itemValue})}
      >
        <Picker.Item label="Google" value="google" />
        <Picker.Item label="Facebook" value="facebook" />
        <Picker.Item label="Twitter" value="twitter" />
      </Picker>
      <Button
        title="Login"
        onPress={() =>
          navigate('Successful')
        }
      />
    </View>  
    );
  }
}
