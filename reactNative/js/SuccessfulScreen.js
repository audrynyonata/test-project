import React from 'react';
import { StyleSheet, Button, Text, TextInput, View, Picker } from 'react-native';
import { StackNavigator } from 'react-navigation';

export default class SucessfulScreen extends React.Component {
  static navigationOptions = { title: 'Social Network Account', header: null };
  render() {
    const { navigate } = this.props.navigation;
    return (
    <View style={{flex:1, alignItems:"center", justifyContent:"center", backgroundColor: "white"}}>
      <Text style={{padding:30}}>Registration Success!</Text>
      <Button
        title="Go back"
        onPress={() =>
          navigate('Index')
        }
      />
    </View>  
    );
  }
}
