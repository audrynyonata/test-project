import React from 'react';
import { StyleSheet, Button, Text, TextInput, View, Picker, TouchableNativeFeedback } from 'react-native';
import { StackNavigator } from 'react-navigation';
import SocialScreen from './SocialScreen';
import SuccessfulScreen from './SuccessfulScreen';

export default class VerificationScreen extends React.Component {
  static navigationOptions = { title: 'Verification', header: null };
  state = {
    digit1:'',
    digit2:'',
    digit3:'',
    digit4:'',
  }
  
  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.container}>
        <View style={[styles.modal, {height: '100%'}]}>
            <Text style={[styles.title, {display:this.state.verificationDisplay}]}>Ketik kode verifikasi</Text>
            <View style={styles.content}>


            <TextInput maxLength={1} textAlignVertical="top" textAlign="center" style={{width:'15%', paddingBottom: 12, fontSize:24, color: 'grey'}}
            onChangeText={(digit1) => this.setState({digit1})}/>
            
            <TextInput maxLength={1} textAlignVertical="top" textAlign="center" style={{width:'15%', paddingBottom: 12, fontSize:24, color: 'grey'}}
            onChangeText={(digit2) => this.setState({digit2})}/>
            
            <TextInput maxLength={1} textAlignVertical="top" textAlign="center" style={{width:'15%', paddingBottom: 12, fontSize:24, color: 'grey'}}
            onChangeText={(digit3) => this.setState({digit3})}/>
            
            <TextInput maxLength={1} textAlignVertical="top" textAlign="center" style={{width:'15%', paddingBottom: 12, fontSize:24, color: 'grey'}}
            onChangeText={(digit4) => this.setState({digit4})}/>
            </View>
            <View style={styles.button}>
                <TouchableNativeFeedback style={{height: 100, marginTop: 10, alignItems:"flex-start"}}
                  onPress={() =>
                    navigate('Successful')
                  } >
                  <Text style={{fontSize: 40, backgroundColor: "#4cb5ab", color:"white"}}> > </Text>
                </TouchableNativeFeedback>
            </View>  
            <View style={[styles.button, {padding:30, alignItems:"flex-start"}]}>
              <TouchableNativeFeedback 
              onPress={() =>
                navigate('Index')
              }>
              <Text style={{fontSize:18, color: "#4a90e2"}}>Kirim ulang kode verifikasi</Text>
              </TouchableNativeFeedback>
          </View>

        </View>   
      </View>
    )
  };
}

const styles = StyleSheet.create({
  container:{
    flex :1,
    flexDirection: 'row',
    backgroundColor : "#4a90e2",
    alignItems: 'flex-end',
  },
  modal: {
    paddingTop: 40,
    width: '100%',
    backgroundColor:'white',
    flexDirection: 'column',
    alignItems: 'flex-start',
  },
  title: { 
    marginLeft: 10,
    marginBottom: 16,
    fontSize: 28,
    color: 'grey',
  },
  content: {
    flexDirection: 'row',
    paddingBottom: 36,
    marginRight: 10,
    padding: 20,
    paddingTop:60,
  },
  button: {
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    flexDirection: 'column',
    width: '100%'
  },
  
})
