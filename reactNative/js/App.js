import React from 'react';

import { Modal, TouchableWithoutFeedback, TouchableNativeFeedback,BackHandler, Picker, Button, Text, Item, TextInput, Image, View, KeyboardAvoidingView, StyleSheet } from 'react-native';
import { StackNavigator} from 'react-navigation';
import VerificationScreen from './VerificationScreen';
import SocialScreen from './SocialScreen';
import SuccessfulScreen from './SuccessfulScreen';

class MainScreen extends React.Component {
  static navigationOptions = { title: 'Welcome', header: null };
  state={
    selected: 'id',
    text: 'Ketik nomor hape',
    height:  240,
    socialDisplay: 'flex',
    verificationDisplay: 'none',
  };
  onFocus() {
    this.setState({height: '100%'});
    this.setState({socialDisplay: 'none'});
    this.setState({verificationDisplay: 'flex'});
    };
  onBlur() {
    this.setState({height: 240});
    this.setState({socialDisplay: 'flex'});
    this.setState({verificationDisplay: 'none'});
    };
  render() {
  const { navigate } = this.props.navigation;
  BackHandler.addEventListener("hardwareBackPress", () => {
      if (this.state.verificationDisplay=='flex') {
      this.setState({height: 240});
      this.setState({socialDisplay: 'flex'});
      this.setState({verificationDisplay: 'none'});     
      return true
    } else {
      return false // exit app
    }
  })
    return (
      <View style={styles.container}>
        <TouchableWithoutFeedback 
          onPress={() => this.onFocus()}
        >
          <View style={[styles.modal, {height: this.state.height}]}>
          <Text style={[styles.title, {display:this.state.socialDisplay}]}>Cari Kerja Pabrik</Text>
          <Text style={[styles.title, {display:this.state.verificationDisplay}]}>Ketik nomor hape</Text>
            <View style={styles.content}>
              <Image style={{width: 40, height: 30, flex:2}}source={{uri: 'https://github.com/hjnilsson/country-flags/blob/master/png100px/'+this.state.selected+'.png'}} />
              <Picker style={{width:'30%'}}
                selectedValue={this.state.selected}
                onValueChange={(itemValue, itemIndex) => this.setState({selected: itemValue})}
              >
                <Picker.Item label="+62" value="id" />
              </Picker>
              <TextInput style={{width:'50%', fontSize:18, color: 'grey'}}
                onBlur={ () => this.onBlur() }
                onFocus={ () => this.onFocus()}
                onPress={() => this.onFocus()}
                onChangeText={(text) => this.setState({text})} value={this.state.text}/>
            </View>  
            <View style={[styles.button, {display: this.state.socialDisplay}]}>
                <TouchableNativeFeedback 
                  onPress={() =>
                  navigate('Social')
                }>
                <Text style={{fontSize:18, color: "#4a90e2"}}>Atau masuk dengan akun media sosial</Text>
                </TouchableNativeFeedback>
            </View>
            <View style={[styles.button, {display: this.state.verificationDisplay, alignItems: 'flex-end'}]}>
                <TouchableNativeFeedback 
                  style={{height: 100, marginTop: 10, width:'100%' }}
                  onPress={() =>
                    navigate('Verification')
                  } >
                  <Text style={{fontSize: 40, backgroundColor: "#4cb5ab", color:"white"}}> > </Text>
                </TouchableNativeFeedback>
                
            </View>
        </View>
        </TouchableWithoutFeedback>    
      </View>
    )
  }
}

const AppNavigator = StackNavigator({
  Index: { screen: MainScreen},
  Social: { screen: SocialScreen },
  Verification: { screen: VerificationScreen },
  Successful: { screen: SuccessfulScreen },
});
export default () => <AppNavigator persistenceKey="NavState" />;

const styles = StyleSheet.create({
  container:{
    flex :1,
    flexDirection: 'row',
    backgroundColor : "#4a90e2",
    alignItems: 'flex-end',
  },
  modal: {
    paddingTop: 40,
    width: '100%',
    backgroundColor:'white',
    flexDirection: 'column',
    alignItems: 'flex-start',
  },
  title: { 
    marginLeft: 10,
    marginBottom: 16,
    fontSize: 28,
    color: 'grey',
  },
  content: {
    flexDirection: 'row',
    paddingBottom: 36,
    marginRight: 10
  },
  button: {
    marginTop: 20,
    justifyContent: 'flex-end',
    alignItems: 'center',
    flexDirection: 'column',
    width: '100%'
  },
  
})
